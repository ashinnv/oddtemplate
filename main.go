package oddtemplate

import (
	"embed"
	"net/http"
)

//go:embed htm/*
var htm embed.FS

func HandleCss() func(wrt http.ResponseWriter, r *http.Request) {
	return func(wrt http.ResponseWriter, r *http.Request) {
		styleDat, _ := htm.ReadFile("htm/css.css") //No error handling because that shouldn't be a problem post-compilation
		wrt.Header().Set("Content-Type", "text/css")
		wrt.Write(styleDat)
	}
}

func HandleJs() func(wrt http.ResponseWriter, r *http.Request) {
	return func(wrt http.ResponseWriter, r *http.Request) {
		styleDat, _ := htm.ReadFile("htm/js.js")
		wrt.Header().Set("Content-Type", "text/javascript")
		wrt.Write(styleDat)
	}
}

func HandleJq() func(wrt http.ResponseWriter, r *http.Request) {
	return func(wrt http.ResponseWriter, r *http.Request) {
		styleDat, _ := htm.ReadFile("htm/jq.js")
		wrt.Header().Set("Content-Type", "text/javascript")
		wrt.Write(styleDat)
	}
}

func HandleRoot() func(wrt http.ResponseWriter, r *http.Request) {
	return func(wrt http.ResponseWriter, r *http.Request) {
		styleDat, _ := htm.ReadFile("htm/index.html")
		wrt.Header().Set("Content-Type", "text/html")
		wrt.Write(styleDat)
	}
}
